use zerocopy::FromBytes;

use std::env;
use std::io::{Read, Write};
use std::net::TcpStream;
use std::thread;

#[derive(Debug, Copy, Clone, FromBytes)]
#[repr(packed)]
pub struct Player {
    pub number: usize,
    pub x: isize,
    pub y: isize,
}

static mut PLAYERS: [Player; 5] = [Player {
    number: 255,
    x: 0,
    y: 0,
}; 5];
static mut COMMANDS: Vec<[u8; 2]> = vec![];
static mut DONE: bool = false;

fn exit() {
    unsafe {
        DONE = true;
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("usage: dropkick [ADDRESS:PORT]");
        std::process::exit(1);
    }

    let number;
    {
        let mut stream = TcpStream::connect(&args[1]).unwrap();

        // send the connect action
        stream.write(&[0, 4]).unwrap();

        // get back player number
        let mut buf = [255; 1];
        stream.read(&mut buf).unwrap();

        number = buf[0];
    }

    ncurses::initscr();
    ncurses::keypad(ncurses::stdscr(), true);
    ncurses::nonl();
    ncurses::cbreak();
    ncurses::noecho();
    ncurses::nodelay(ncurses::stdscr(), true);
    ncurses::curs_set(ncurses::CURSOR_VISIBILITY::CURSOR_INVISIBLE);

    let ip1 = args[1].clone();
    let write = thread::spawn(move || loop {
        if unsafe { DONE } {
            break;
        }
        if unsafe { COMMANDS.len() } == 0 {
            continue;
        }

        let mut stream = TcpStream::connect(&ip1).expect("Failed to connect to TCP stream");

        // send all queued commands
        while let Some(top) = unsafe { COMMANDS.pop() } {
            stream.write(&top).expect("Failed to send over TCP");
        }
    });

    let ip2 = args[1].clone();
    let read = thread::spawn(move || loop {
        if unsafe { DONE } {
            break;
        }
        let mut stream = TcpStream::connect(&ip2).expect("Failed to connect to TCP stream");

        stream.write(&[number, 6]).expect("Failed to send over TCP");

        let mut buffer = Vec::new();
        match stream.read_to_end(&mut buffer) {
            Ok(_) => {}
            Err(_) => {}
        };

        // create a slice
        let buffer = &buffer[..];

        // convert the bytes to a Player
        unsafe {
            PLAYERS = *(buffer.as_ptr() as *mut [Player; 5]);
        }
    });

    let tui = thread::spawn(move || loop {
        if unsafe { DONE } {
            break;
        }
        ncurses::clear();
        unsafe {
            let f = PLAYERS
                .iter()
                .filter(|p| p.number != 255)
                .collect::<Vec<&Player>>();
            for player in f {
                ncurses::mvprintw(player.x as i32, player.y as i32, "@");
            }
        }
        thread::sleep(std::time::Duration::from_millis(1));
    });

    let commands = thread::spawn(move || {
        loop {
            if unsafe { DONE } {
                break;
            }
            let action = match ncurses::getch() as u8 as char {
                // exit
                'q' => {
                    unsafe { COMMANDS.push([number, 5]) };
                    exit();
                    break;
                }

                // up
                'w' => 0,

                // down
                's' => 1,

                // left
                'a' => 2,

                // right
                'd' => 3,

                // ignore
                _ => continue,
            };

            unsafe { COMMANDS.push([number, action]) };
        }
    });

    read.join().unwrap();
    write.join().unwrap();
    tui.join().unwrap();
    commands.join().unwrap();

    exit();
    ncurses::endwin();
}
