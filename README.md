# dropkick
an online game experiment

## building
you'll need
- [rust](https://www.rust-lang.org/)
- [ncurses](https://invisible-island.net/ncurses/)

then run `make` or `cargo build` to build the server software (`dropkickd`) and the client (`dropkick`)
