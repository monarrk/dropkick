target/debug/dropkick: dropkick/src/* dropkickd/src/*
	cargo build

run: target/debug/dropkick
	target/debug/dropkickd

clean:
	cargo clean
