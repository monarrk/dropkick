use std::fmt;

use async_std::net::{TcpListener, TcpStream};
use async_std::task::spawn;
use futures::{AsyncReadExt, AsyncWriteExt, StreamExt};
use zerocopy::AsBytes;

/// an error
#[derive(Debug)]
pub enum DropkickError {
    InvalidAction,
}

impl std::error::Error for DropkickError {}

impl fmt::Display for DropkickError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DropkickError::InvalidAction => write!(f, "Invalid Action"),
        }
    }
}

/// Actions a player can take
#[derive(Debug, Copy, Clone)]
pub enum Action {
    Up,
    Down,
    Left,
    Right,
    Connect,
    Disconnect,
    Request,
}

/// a player in the game
/// must implement AsBytes to be sent over TCP
#[derive(Debug, Copy, Clone, AsBytes)]
#[repr(packed)]
pub struct Player {
    pub number: usize,
    pub x: isize,
    pub y: isize,
}

impl Player {
    pub fn new(number: usize) -> Self {
        Self { number, x: 0, y: 0 }
    }
}

#[derive(Debug)]
pub struct Command {
    player: usize,
    action: Action,
}

impl Command {
    pub fn new(player: u8, action: u8) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Self {
            player: player as usize,
            action: action_from_u8(action)?,
        })
    }

    pub fn player(&self) -> usize {
        self.player
    }
    pub fn action(&self) -> Action {
        self.action
    }
}

/// Convert a u8 from the UDP connection to an Action
pub fn action_from_u8(action: u8) -> Result<Action, Box<dyn std::error::Error>> {
    match action {
        0 => Ok(Action::Up),
        1 => Ok(Action::Down),
        2 => Ok(Action::Left),
        3 => Ok(Action::Right),
        4 => Ok(Action::Connect),
        5 => Ok(Action::Disconnect),
        6 => Ok(Action::Request),
        _ => Err(Box::new(DropkickError::InvalidAction)),
    }
}

async unsafe fn handle_connection(mut stream: TcpStream) {
    let mut buf = [0, 0];
    match stream.read_exact(&mut buf).await {
        Ok(o) => o,
        Err(e) => {
            eprintln!("Error recieving from socket: {}", e);
            return;
        }
    };

    // make a command structure
    let cmd = match Command::new(buf[0], buf[1]) {
        Ok(c) => c,
        Err(e) => {
            eprintln!("Error creating command: {}", e);
            return;
        }
    };

    // execute the action
    match cmd.action() {
        Action::Request => {}
        Action::Connect => {
            IDX += 1;
            println!("Connecting player {}", IDX);
            PLAYERS[IDX] = Player::new(IDX);

            // send the player number
            match stream.write(&[IDX.clone() as u8]).await {
                Ok(_) => {}
                Err(e) => {
                    eprintln!("Error sending player number: {}", e);
                    return;
                }
            };
        }
        Action::Disconnect => {
            println!("Disconnecting player {}", cmd.player());
            PLAYERS[cmd.player()] = Player::new(255);
            IDX -= if IDX == 0 { 0 } else { 1 };
        }
        Action::Up => PLAYERS[cmd.player()].x -= 1,
        Action::Down => PLAYERS[cmd.player()].x += 1,
        Action::Left => PLAYERS[cmd.player()].y -= 1,
        Action::Right => PLAYERS[cmd.player()].y += 1,
    }

    // println!("{} Sending {:?} over TCP", ITER, PLAYERS);
    match stream.write_all(PLAYERS.as_bytes()).await {
        Ok(_) => {}
        Err(e) => {
            eprintln!("Failed to send PLAYERS over the connection: {}", e);
            return;
        }
    };

    match cmd.action() {
        Action::Request => {}
        _ => println!("{:?}", cmd),
    }

    ITER += 1;
}

static mut PLAYERS: [Player; 5] = [Player {
    number: 255,
    x: 0,
    y: 0,
}; 5];
static mut IDX: usize = 0;
static mut ITER: u64 = 0;

#[async_std::main]
async fn main() {
    let listener = TcpListener::bind("0.0.0.0:38799").await.unwrap();

    println!("Started TCP socket; waiting for connections");

    listener
        .incoming()
        .for_each_concurrent(None, |stream| async move {
            let stream = match stream {
                Ok(s) => s,
                Err(e) => {
                    eprintln!("Error unwrapping stream: {}", e);
                    return;
                }
            };
            unsafe { spawn(handle_connection(stream)) };
        })
        .await;
}
